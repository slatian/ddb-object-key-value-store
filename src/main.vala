
public static MainLoop? loop = null;

// This is used to ignore replys to own events
public static string? last_sent_value = null;

public static int main(string[] args) {
	
	string command = "server";

	if (args.length >= 2) {
		command = args[1];
	} else {
		command = "help";
	}
	
	if (command == "help") {
		print_usage();
		return 0;
	}
	
	string? port_str = GLib.Environment.get_variable("DDB_PORT");
	string? unix_socket_path = GLib.Environment.get_variable("DDB_SOCKET_PATH");
	string? greeting = GLib.Environment.get_variable("DDB_GREETING");
	
	if ((port_str == null && unix_socket_path == null) || greeting == null) {
		message("Missing Environment Variables, see 'ddb help'.\n");
		return 1;
	}
	
	uint16 port = 0;
	
	// Only try to evaluate the port number if given
	if (port_str != null) {
		uint64 port_parse_result = 0;
		if (DDB.Intparser.try_parse_unsigned(port_str, out port_parse_result)) {
			port = (uint16) port_parse_result;
		}

		if (port == 0) {
			message("Port must be a valid integer > 0!\n");
			return 1;
		}
	}
	
	SocketAddress[] socket_addresses = {};
	SocketAddress? primary_socket_address = null;
	
	if (port > 0) {
		if (command == "server") { message(@"Adding Tcp socket at: $port@127.0.0.1\n"); }
		var tcp_socket_address = new InetSocketAddress.from_string("127.0.0.1", port);
		socket_addresses += tcp_socket_address;
		primary_socket_address = tcp_socket_address;
	}
	
	if (unix_socket_path != null) {
		if (command == "server") { message(@"Adding Unix socket at: $unix_socket_path\n"); }
		var unix_socket_address = new UnixSocketAddress(unix_socket_path);
		socket_addresses += unix_socket_address;
		primary_socket_address = unix_socket_address;
	}
	
	if (primary_socket_address == null) {
		message("No Connection Method was specified!\n");
		return 1;
	}
	
	GLib.Unix.signal_add(2, sigtermhandler);
	GLib.Unix.signal_add(15, sigtermhandler);
	
	switch (command) {
	case "server":
		int returnval = main_server(socket_addresses, greeting);
		// Clean up the unix socket if everything else was successful
		if (unix_socket_path != null && returnval != 2 && returnval != 1) {
			message("Cleaning up Unix socket …\n");
			GLib.FileUtils.remove(unix_socket_path);
		}
		return returnval;
	case "cat":
		return main_cat(primary_socket_address, greeting);
	case "read_value":
	case "write_value":
	case "value":
		if (args.length < 4) {
			print_usage();
			return 1;
		}
		
		bool do_initial_read = command == "read_value";
		bool subscribe = false;
		bool no_output_on_unset = false;
		string? initial_write = null;
		bool do_write_loop = false;
		bool no_unset = false;
		bool try_ignore_self = false;
		int start_options_at = 4;
		
		if (command == "write_value") {
			if (args.length < 5) {
				print_usage();
				return 1;
			} else {
				initial_write = args[4];
			}
			start_options_at = 5;
		}
		
		for (int i = start_options_at; i < args.length; i++) {
			switch(args[i]) {
			case "--subscribe":
				subscribe = true;
				break;
			case "--no-output-on-unset":
				no_output_on_unset = true;
				break;
			case "--initial-read":
				do_initial_read = true;
				break;
			case "--no-initial-read":
				do_initial_read = false;
				break;
			case "--pipe-out":
				do_initial_read = true;
				subscribe = true;
				break;
			case "--pipe-in":
				do_write_loop = true;
				break;
			case "--no-unset":
				no_unset = true;
				break;
			case "--try-ignore-self":
				try_ignore_self = true;
				break;
			default:
				print_usage();
				return 1;
			}
		}
		return main_read_value(args[2], args[3], primary_socket_address, greeting, do_initial_read, subscribe, no_output_on_unset, initial_write, do_write_loop, no_unset, try_ignore_self);
	case "wrap":
		if (args.length <= 2) {
			print_usage();
			return 1;
		}
		string[] wrapped_command = new string[args.length-2];
		for (int i = 0; i < wrapped_command.length; i++){
			wrapped_command[i] = args[i+2];
		}
		return main_wrap(wrapped_command, primary_socket_address, greeting);
	default:
		message("Unknown command!\n");
		print_usage();
		return 1;
	}

}

public static bool sigtermhandler() {
	message("Received SIGTERM or SIGINT!\n");
	if (loop != null) {
		message("Quitting loop …\n");
		loop.quit();
		return true;
	} else {
		return false;
	}
}

public static void message(string message) {
	stderr.write(message.data);
}

public static void output(string message) {
	stdout.write(message.data);
}

public static void print_usage() {
	message("""Usage: ddb <command>
Avalable commands are:
	help - show this text
	server - starts a ddb server
	cat - starts a ddb client in cat mode
	read_value <o> <k> <<options>> - reads a value from ddb to stdout
	write_value <o> <k> <v> <<options>> - write a value to ddb from argument
	value <o> <k> <<options>> - blank value operation, use with options
		--subscribe - enters a loop that prints updates (after the initial query)
		--no-output-on-unset - does not print an update if value gets unset
		--initial-read - does an initial read on the value (default for read_value)
		--no-initial-read - disables the initial read on the value
		--pipe-out - equivatent to --initial-read and --subscribe
		--pipe-in - starts a read loop that writes values to stdin
		--no-unset - ignores empty lines on stdin
		--try-ignore-self - Try to ignore events triggered by own events
			Useful when used with --pipe-in and --pipe-outs
	wrap <<command>> - wraps a command in a ddb connection
DDB takes arguments via envoirenment variables:
	DDB_SOCKET_PATH: The path to the ddb unix socket
	DDB_PORT: The port ddb will listen on
	DDB_GREETING: The greeting ddb will accept

Note: If DDB_SOCKET_PATH is present it will, for clients
      be the preferred and only way to connect to ddb.
""");
}

//****************************************************
//* NOTE: Only call one of the main functions below, *
//*    They all use the global variable loop to      *
//*    store their mainloop!                         *
//*    This is because of the quit signal handler.   *
//****************************************************

public static int main_server(SocketAddress[] addresses, string greeting) {
	output(@"Listening for greeting '$greeting'!\n");
	var server = new DDB.Server(addresses, greeting, new DDB.Store());
	if (!server.start()) {
		message("An error occourred while starting the server! (see above)\n");
		return 2;
	}
	loop = new MainLoop();
	loop.run();
	message("Cleaning up …\n");
	server.socket_service.stop();
	server.socket_service.close();
	return 0;
}

public static int main_cat(SocketAddress address, string greeting) {
	int returnval = 0;
	loop = new MainLoop();
	var stdinstream = new DataInputStream(new UnixInputStream(stdin.fileno(), false));
	var client = new DDB.ClientSocket(address);
	bool is_first_line = true;
	client.on_line_received.connect((line) => {
		if (is_first_line) {
			if (line != "Hello!") {
				client.close();
				returnval = 3;
				loop.quit();
			}
			is_first_line = false;
		}
		if (returnval == 0) {
			output(line+"\n");
		}
	});
	if (!client.initiate_connection()) {
		return 2;
	}
	client.send(greeting+"\n");
	cat_read_loop.begin(client, stdinstream, loop);
	client.read_loop.begin((obj, res) => {loop.quit();});
	loop.run();
	client.close();
	return returnval;
}

public static async void cat_read_loop(DDB.ClientSocket client, DataInputStream stdinstream, MainLoop loop) {
	string? line = null;
	try {
		while (true) {
			line = yield stdinstream.read_line_utf8_async();
			if (line == null) { break; }
			client.send(line+"\n");
			if (line == "q") { break; }
		}
	} catch (Error e) {
		//Do nothing
	}
	loop.quit();
}

public static int main_wrap(string[] args, SocketAddress address, string greeting) {
	int returnval = 0;
	loop = new MainLoop();
	
	Pid child_pid = -1;
	int stdinfd = -1;
	int stdoutfd = -1;
	
	
	DataInputStream read_stream = null;
	DataOutputStream write_stream = null;
	var client = new DDB.ClientSocket(address);
	bool is_first_line = true;
	client.on_line_received.connect((line) => {
		if (is_first_line) {
			if (line == "Hello!") {
				try {
					Process.spawn_async_with_pipes(null, args, null, SEARCH_PATH, null, out child_pid, out stdinfd, out stdoutfd);
					read_stream = new DataInputStream(new UnixInputStream(stdoutfd, false));
					write_stream = new DataOutputStream(new UnixOutputStream(stdinfd, false));
					cat_read_loop.begin(client, read_stream, loop);
				} catch (Error e) {
					client.close();
					returnval = 5;
					loop.quit();
				}
			} else {
				client.close();
				returnval = 3;
				loop.quit();
			}
			is_first_line = false;
		}
		if (returnval == 0) {
			try {
				if (write_stream != null) {
					write_stream.put_string(line+"\n");
				}
			} catch (Error e) {
				client.close();
				loop.quit();
			}
		}
	});
	if (!client.initiate_connection()) {
		return 2;
	}
	client.send(greeting+"\n");
	client.read_loop.begin((obj, res) => {loop.quit();});
	loop.run();
	try {
		if (write_stream != null) {
			write_stream.close();
		}
	} catch (Error e) {}
	try {
		if (read_stream != null) {
			read_stream.close();
		}
	} catch (Error e) {}
	client.close();
	return returnval;
}

public static int main_read_value(string object, string key, SocketAddress address, string greeting, bool do_initial_read, bool subscribe, bool no_output_on_unset, string? initial_write, bool do_write_loop, bool no_unset, bool try_ignore_self) {
	if (object.contains(" ") || object.contains("\n") ||
	    key.contains(" ") || key.contains("\n")) {
		return 1;
	}
	if (initial_write != null) {
		if (initial_write.contains("\n")) {
			return 1;
		}
	}
	int returnval = 0;
	loop = new MainLoop();
	var client = new DDB.ClientSocket(address);
	string object_and_key = @"$object $key";
	bool is_first_line = true;
	bool initial_write_to_be_done = initial_write != null;
	client.on_line_received.connect((line) => {
		bool quit = false;
		if (is_first_line) {
			if (line != "Hello!") {
				client.close();
				returnval = 3;
				loop.quit();
			} else {
				if (do_initial_read) {
					client.send(@"r $object_and_key\n");
				} else if (initial_write != null) {
					client.send_value_update(object_and_key, initial_write);
					initial_write_to_be_done = false;
				}
				if (subscribe) {
					client.send(@"+ $object\n");
				}
			}
			is_first_line = false;
		} else if (line == "ERROR") {
			returnval = 4;
			quit = true;
		} else if (line == "OK") {
			quit = !(do_initial_read || subscribe || do_write_loop);
			//Ignore otherwise
		} else if (line.has_prefix("> ") && (do_initial_read || subscribe)) {
			var args = line.split(" ", 4);
			if (args.length == 4) {
				if (args[1] == object && args[2] == key) {
					if (try_ignore_self && args[3] == last_sent_value) {
						last_sent_value = null;
					} else {
						output(args[3]+"\n");
						quit = !subscribe;
						do_initial_read = false;
						if (initial_write_to_be_done) {
							client.send_value_update(object_and_key, initial_write);
							initial_write_to_be_done = false;
						}
					}
				}
			} else {
				returnval = 3;
				quit = true;
			}
		} else if (line.has_prefix("u ")) {
			if (line == @"u $object_and_key") {
				if (try_ignore_self && "" == last_sent_value) {
						message("Should have ignored …");
						last_sent_value = null;
				} else {
					if (!no_output_on_unset) {
						output("\n");
					}
					quit = !subscribe;
					do_initial_read = false;
					if (initial_write_to_be_done) {
						client.send_value_update(object_and_key, initial_write);
						initial_write_to_be_done = false;
					}
				}
			}
		}
		if (quit) {
			client.send("q\n");
			loop.quit();
		}
	});
	if (!client.initiate_connection()) {
		return 2;
	}
	client.send(greeting+"\n");
	client.read_loop.begin((obj, res) => {loop.quit();});
	if (do_write_loop) {
		var stdinstream = new DataInputStream(new UnixInputStream(stdin.fileno(), false));
		write_loop.begin(client, stdinstream, loop, object_and_key, no_unset);
	}
	loop.run();
	client.close();
	return returnval;
}

public static async void write_loop(DDB.ClientSocket client, DataInputStream stdinstream, MainLoop loop, string object_and_key, bool no_unset) {
	string? line = null;
	try {
		while (true) {
			line = yield stdinstream.read_line_utf8_async();
			if (line == null) { break; }
			if (!no_unset || line != "") {
				client.send_value_update(object_and_key, line);
				last_sent_value = line;
			}
		}
	} catch (Error e) {
		//Do nothing
	}
	client.send("q");
	loop.quit();
}
