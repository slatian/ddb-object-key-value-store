public class DDB.Store : Object {
	
	public signal void value_updated(string obj_name, string key, string? val);
	public signal void object_created(string obj_name, string obj_type);
	public signal void object_removed(string obj_name);
	public signal void signal_fired(string obj_name, string signal_name);
	
	private HashTable<string, DDB.StoreObject> objects = new HashTable<string, DDB.StoreObject>(str_hash, str_equal);
	
	public bool set_object_property(string obj_name, string key, string? val){
		lock (objects) {
			DDB.StoreObject? obj = objects.get(obj_name);
			if (obj == null) {
				return false;
			} else {
				if (obj.set_object_property(key, val)) {
					this.value_updated(obj_name, key, val);
					return true;
				} else {
					return false;
				}
			}
		}
	}
	
	public string? get_object_property(string obj_name, string key){
		lock (objects) {
			DDB.StoreObject? obj = objects.get(obj_name);
			if (obj == null) {
				return null;
			} else {
				return obj.get_object_property(key);
			}
		}
	}
	
	public bool create_object(string obj_name, string obj_type){
		lock (objects) {
			if (has_object(obj_name)) {
				return false;	
			} else {
				objects.set(obj_name, new DDB.StoreObject(obj_type));
				this.object_created(obj_name, obj_type);
				return true;
			}
		}
	}
	
	public bool remove_object(string obj_name){
		lock (objects) {
			if (has_object(obj_name)) {
				if (objects.remove(obj_name)) {
					this.object_removed(obj_name);
					return true;
				} else {
					return false;
				}
			} else {
				return false;
			}
		}
	}
	
	public bool send_signal(string obj_name, string signal_name){
		lock (objects) {
			if (has_object(obj_name)) {
				signal_fired(obj_name, signal_name);
				return true;
			} else {
				return false;
			}
		}
	}
	
	public bool has_object(string obj_name) {
		return objects.contains(obj_name);
	}
	
	public void foreach_object(HFunc<string, DDB.StoreObject> cb){
		lock (objects) {
			objects.for_each(cb);
		}
	}
	
}
